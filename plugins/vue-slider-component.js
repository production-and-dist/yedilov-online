import Vue from 'vue'
import 'vue-range-component/dist/vue-range-slider.css'
import VueSlider from 'vue-slider-component'

Vue.component('vue-range-slider', VueSlider)
