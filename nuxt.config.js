export default {
	server: {
		// host: "0", // default: localhost
		// port: 8000
	},
	debug: false,
	// Global page headers: https://go.nuxtjs.dev/config-head
	head: {
		title: "YEDILOV ONLINE",
		htmlAttrs: {
			lang: "ru",
		},

		meta: [
			{ charset: "utf-8" },
			{ name: "viewport", content: "width=device-width, initial-scale=1" },
			{
				hid: "og_url",
				name: "og:url",
				content: "https://yedilov.online/",
			},
			{
				hid: "og_title",
				name: "og:title",
				content: "YEDILOV ONLINE ",
			},

			{
				hid: "og_description",
				name: "og:description",
				content:
					"Уникальный проект с биографиями управленческой элиты и персон Республики Казахстан.",
			},

			{
				hid: "og_image",
				name: "og:image",
				content: "https://yedilov.online/thumbnail.png",
			},

			{
				property: "og:image",
				content: "https://yedilov.online/thumbnail.png",
			},
			{
				hid: "description",
				name: "description",
				content:
					"Уникальный проект с биографиями управленческой элиты и персон Республики Казахстан.",
			},
			{
				hid: "twitter_card",
				name: "twitter:card",
				content:
					"Уникальный проект с биографиями управленческой элиты и персон Республики Казахстан.",
			},
			{
				hid: "og_site_name",
				name: "og:site_name",
				content: "YEDILOV ONLINE",
			},
			{
				hid: "twitter_image_alt",
				name: "twitter:image:alt",
				content: "",
			},
			{ name: "format-detection", content: "telephone=no" },
			{ name: "msapplication-TileColor", content: "#da532c" },
			{ name: "theme-color", content: "#ffffff" },
			{
				name: "twitter:image:src",
				content: "https://yedilov.online/thumbnail.png",
			},
			{
				hid: "vk_image",
				property: "vk:image",
				content: "https://yedilov.online/thumbnail.png",
			},
			{
				hid: "twitter_image",
				name: "twitter:image",
				content: "https://yedilov.online/thumbnail.png",
			},
			{
				hid: "twitter_image_alt",
				name: "twitter:image:alt",
				content: "YEDILOV ONLINE IMAGE",
			},
		],
		link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
		link: [
			{
				rel: "icon",
				type: "image/png",
				sizes: "32x32",
				href: "/favicon-32x32.png",
			},
			{
				rel: "icon",
				type: "image/png",
				sizes: "16x16",
				href: "/favicon-16x16.png",
			},
			{
				rel: "icon",
				type: "image/x-icon",
				href: "/apple-touch-icon.png",
			},

			{
				rel: "apple-touch-icon",
				sizes: "60x60",
				href: "/apple-touch-icon.png",
			},
			{
				rel: "apple-touch-icon",
				sizes: "16x16",
				href: "/apple-touch-icon.png",
			},
			{
				rel: "apple-touch-icon",
				sizes: "32x32",
				href: "/apple-touch-icon.png",
			},
			{
				rel: "manifest",
				href: "/site.webmanifest",
			},
			{
				rel: "mask-icon",
				color: "#5bbad5",
				href: "/safari-pinned-tab.svg",
			},
		],
	},

	// Global CSS: https://go.nuxtjs.dev/config-css
	css: ["@/static/css/global.css", "@/static/css/hamburger.css"],

	// Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
	plugins: [
		{
			src: "~plugins/vue-slider-component.js",
			ssr: false,
		},
		{
			src: "~plugins/vue-load-image.js",
			ssr: false,
		},
		// { src: "~/plugins/logo-animation.js", mode: "client" },
		"~/plugins/lodash.js",
	],

	// Auto import components: https://go.nuxtjs.dev/config-components
	components: true,

	// Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
	buildModules: [
		"nuxt-animejs",
		// https://go.nuxtjs.dev/eslint
		// "@nuxtjs/eslint-module",

		"vue-browser-detect-plugin/nuxt",
		"@nuxt/image",
	],

	// Modules: https://go.nuxtjs.dev/config-modules
	modules: [
		// https://go.nuxtjs.dev/bootstrap
		"bootstrap-vue/nuxt",
		// https://go.nuxtjs.dev/axios
		"@nuxtjs/axios",
		"@nuxt/http",
		"@nuxtjs/proxy",
		"@nuxt/image",
		// [
		// 	"nuxt-lazy-load",
		// 	{
		// 		// defaultImage: "~/static/img/loader-img.gif",
		// 	},
		// ],
		"@nuxtjs/robots",
	],
	image: {
		presets: {
			cover: {
				modifiers: {
					fit: "cover",
					format: "jpg",
					width: 100,
					height: 100,
				},
			},
		},
	},
	robots: {
		UserAgent: "*",
	},
	// Axios module configuration: https://go.nuxtjs.dev/config-axios
	axios: {
		proxy: true,
	},
	proxy: {
		"/api/": {
			target: "https://api.yedilov.online/",
			pathRewrite: { "^/api/": "" },
			changeOrigin: true,
		},
	},

	// loading: "~/components/loading.vue",

	// Build Configuration: https://go.nuxtjs.dev/config-build
	build: {},
	serverMiddleware: {
		"/_ipx": "~/server/middleware/ipx.js",
	},
};
