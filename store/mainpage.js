import axios from "axios";
export const state = () => ({
	listAll: [],
	list: [],
	item: {},
	profile: {},
	career: {},
	education: {},
	result_count: 0,
	filters: {},
	filterConditions: {},
	filters1: {},
	filters2: {},
	filters3: {},
	user: {},
	filterAge: {
		AGE: {
			NAME: "Возраст",
			LIST: new Set(),
		},
	},
	formStatus: {},
	keys: {},
	mobileMenuOpen: false,
	datalist: [],
	ad: {},
	sample_array: [
		1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5,
		6, 7, 8, 9, 10,
	],
	tariff_list: [],
	faq_list: [],
});

export const mutations = {
	setAuthUser(state, data) {
		state.user = {
			...state.user,
			...data,
		};
	},
	setFormStatus(state, payload) {
		const { name } = payload;
		console.log("dataatatat", payload);
		state.formStatus = {
			...state.formStatus,
			[name]: payload,
		};
	},
	setMobileMenu(state, data) {
		state.mobileMenuOpen = !data;
	},
	clearAuthUser(state) {
		state.user = null;
	},
	setTariffList(state, data) {
		state.tariff_list = data;
	},
	setFAQList(state, data) {
		state.faq_list = data;
	},

	setPersonsList(state, data) {
		state.listAll = data;
		state.list = data;
		state.result_count = data ? data.length : 0;
	},
	setPersonItem(state, data) {
		state.item = data[0];
	},
	setPersonProfile(state, data) {
		state.profile = data[0];
	},
	setPersonCareer(state, data) {
		state.career = data[0];
	},
	setPersonEducation(state, data) {
		state.education = data[0];
	},
	setAgeFilter(state) {
		for (let item in state.listAll) {
			for (let key in state.filterAge) {
				if (state.listAll[item][key] != undefined) {
					state.filterAge[key]["LIST"].add(state.listAll[item][key]);
				}
			}
		}
	},
	setFilters(state, data) {
		let filters1 = {};
		let filters2 = {};
		let filters3 = {};
		for (let key in data) {
			let has_child = false;
			let has_gender = false;
			for (let item in data[key]["LIST"]) {
				if (data[key]["LIST"][item]["CHILD"] != undefined) {
					has_child = true;
					break;
				} else if (data[key]["LIST"][item]["GENDER"] != undefined) {
					has_gender = true;
					break;
				}
			}
			if (has_child) {
				filters1[key] = data[key];
			} else if (has_gender) {
				filters3[key] = data[key];
			} else {
				filters2[key] = data[key];
			}
		}

		let result2 = Object.keys(filters2).map((key) => [key, filters2[key]]);

		state.filters2 = Object.fromEntries(result2.reverse());

		state.filters1 = filters1;
		state.filters3 = filters3;
	},
	setFilter(state, data) {
		// console.log("setting filter", data);
		const filter = JSON.parse(data || "{}");

		for (let f in filter) {
			// console.log("kazakh", filter[f], f);
			if (filter[f] == "Выбрать") {
				if (f == "NATIONALITY") {
					state.filterConditions[f + "_GENDER"] = "";
					state.filterConditions[f + "_GENDER_SECTION"] = "";
				}
				state.filterConditions[f] = "";
				state.filterConditions[f + "_SECTION"] = "";
				break;
			} else if (filter[f] == "ALL") {
				filter[f] = "";
			} else if (filter[f] == "CLEAR_ALL") {
				state.filterConditions = {};
				break;
			} else if (filter[f] == "Казах") {
				state.filterConditions[f + "_GENDER"] = "";
				state.filterConditions[f + "_GENDER_SECTION"] = "";
			}

			state.filterConditions[f] = filter[f];
		}
		// console.log("filters", state.filterConditions);
	},
	setResultList(state, data) {
		state.list = data;
		state.result_count = data.length;
	},
	setKeys(state, data) {
		state.keys = data;
	},
	setDatalist(state, data) {
		state.datalist = data;
		// console.log("DATALIST", data);
	},
	setAd(state, data) {
		state.ad = data ? data[0] : {};
	},
};

export const actions = {
	mobileMenu({ commit }, mobileMenu) {
		commit("setMobileMenu", mobileMenu);
	},
	async fetchUser({ commit, dispatch }) {
		if (!process.client) return;

		try {
			const url = process.server
				? "https://api.yedilov.online/mobileapp/api/profile"
				: "/api/mobileapp/api/profile";
			const token = localStorage.getItem("token");
			this.$axios.setHeader("Token", token);

			const response = await this.$axios.$post(url);
			if (response.errors) {
				throw new Error("AUTH");
			} else {
				if (response.login) {
					localStorage.setItem("login", response.login);
				}
				commit("setAuthUser", {
					signed: true,
					...response,
				});
			}
		} catch (error) {
			try {
				const url = process.server
					? "https://api.yedilov.online/mobileapp/api/profile/edit"
					: "/api/mobileapp/api/refreshtoken";
				const token = localStorage.getItem("token");
				const refresh_token = localStorage.getItem("refresh_token");

				this.$axios.setHeader("Token", token);

				const response = await this.$axios.$post(
					url,
					JSON.stringify({
						refresh_token,
					})
				);
				if (response.errors) {
					throw new Error("AUTH");
				} else {
					localStorage.setItem("signed", true);
					localStorage.setItem("token", response.token);
					localStorage.setItem("refresh_token", response.refresh_token);
					commit("setAuthUser", {
						signed: true,
					});
				}
			} catch (e) {
				dispatch("clearAuthUser");
			}
		}
	},
	clearAuthUser({ commit }) {
		if (process.client) {
			localStorage.removeItem("signed");
			localStorage.removeItem("token");
			localStorage.removeItem("refresh_token");
			localStorage.removeItem("login");
		}

		commit("clearAuthUser");
	},
	async fetchPersonsList({ commit }) {
		const url = process.server
			? "https://api.yedilov.online/mobileapp/api/persons/"
			: "/api/mobileapp/api/persons/";

		await this.$axios
			.$get(url)
			.then((response) => {
				return response.result;
			})
			.then((data) => {
				commit("setPersonsList", data);
				commit("setAgeFilter");
			});
	},

	async fetchPersonItem({ commit }, id) {
		let url = process.server
			? "https://api.yedilov.online/rest/persons/" + id
			: "/api/rest/persons/" + id;
		this.$axios.setHeader("Access-Control-Allow-Origin", "*");
		this.$axios.setHeader("X-Requested-With", "XMLHttpRequest");
		await this.$axios
			.$get(url)
			.then((response) => {
				return response.result;
			})
			.then((data) => {
				commit("setPersonItem", data);
			});
	},
	async fetchPersonItemProfile({ commit }, id) {
		let url = process.server
			? "https://api.yedilov.online/rest/persons/profile/" + id
			: "/api/rest/persons/profile/" + id;
		this.$axios.setHeader("Access-Control-Allow-Origin", "*");
		this.$axios.setHeader("X-Requested-With", "XMLHttpRequest");
		await this.$axios
			.$get(url)
			.then((response) => {
				return response.result;
			})
			.then((data) => {
				commit("setPersonProfile", data);
			});
	},
	async fetchPersonItemCareer({ commit }, id) {
		let url = process.server
			? "https://api.yedilov.online/rest/persons/career/" + id
			: "/api/rest/persons/career/" + id;
		this.$axios.setHeader("Access-Control-Allow-Origin", "*");
		this.$axios.setHeader("X-Requested-With", "XMLHttpRequest");
		await this.$axios
			.$get(url)
			.then((response) => {
				return response.result;
			})
			.then((data) => {
				commit("setPersonCareer", data);
			});
	},
	async fetchPersonItemEducation({ commit }, id) {
		let url = process.server
			? "https://api.yedilov.online/rest/persons/education/" + id
			: "/api/rest/persons/education/" + id;
		this.$axios.setHeader("Access-Control-Allow-Origin", "*");
		this.$axios.setHeader("X-Requested-With", "XMLHttpRequest");
		await this.$axios
			.$get(url)
			.then((response) => {
				return response.result;
			})
			.then((data) => {
				commit("setPersonEducation", data);
			});
	},

	async fetchFilters({ commit }) {
		let url = process.server
			? "https://api.yedilov.online/rest/filter/"
			: "/api/rest/filter/";
		this.$axios.setHeader("Access-Control-Allow-Origin", "*");
		this.$axios.setHeader("X-Requested-With", "XMLHttpRequest");
		await this.$axios
			.$get(url)
			.then((response) => {
				return response.result;
			})
			.then((data) => {
				commit("setFilters", data);
			});
	},
	filterResult({ commit, state }, data) {
		// console.log("filterting", data, state.filterConditions);
		commit("setFilter", data);

		const list = state.listAll;
		const filters = state.filterConditions;
		// console.log("setFilter", list);
		let new_datalist = [];
		const wordsValidate = (
			words,
			payload = [],
			field = "NAME",
			filter_item = {},
			filter_item_field = ""
		) => {
			if (Array.isArray(payload)) {
				let check = false;

				for (const item of payload) {
					const inner_check = words.reduce((acc, i) => {
						if (new RegExp(`${i}`, "gi").test(item[field])) {
							// console.log("educ", i);
							new_datalist.unshift(item[field]);
						}

						return acc + new RegExp(`${i}`, "gi").test(item[field]);
					}, false);

					if (inner_check == words.length) {
						check = true;
						// console.log(filter_item, "ITEM");
						filter_item["SEARCH"] = {};
						filter_item["SEARCH"][filter_item_field] = item[field];

						break;
					}
				}

				return check;
			} else {
				const validated_word_count = words.reduce((acc, i) => {
					if (new RegExp(`${i}`, "gi").test(payload)) {
						filter_item["SEARCH"] = {};
						filter_item["SEARCH"][filter_item_field] = payload;
					}

					return acc + new RegExp(`${i}`, "gi").test(payload);
				}, false);

				return validated_word_count == words.length;
			}
		};
		const result = list.filter((item) => {
			let matched = true;

			for (let f in filters) {
				if (filters[f]) {
					if (["FULL_NAME", "ORGANIZATION", "EDUCATION"].includes(f)) {
						const words = filters[f].split(" ").filter((w) => !!w);
						let validation_successed = true;
						// console.log(words, "CHECK SEARCH");
						if (f === "FULL_NAME") {
							validation_successed = wordsValidate(
								words,
								item[f],
								"NAME",
								item,
								"FULL_NAME"
							);
						} else if (f === "ORGANIZATION") {
							let payload;

							if (item[f] && item[f]["NAME"]) {
								payload = item[f]["NAME"];
							}

							validation_successed = wordsValidate(
								words,
								payload,
								"NAME",
								item,
								"ORGANIZATION"
							);
						} else if (f === "EDUCATION") {
							validation_successed = wordsValidate(
								words,
								item[f],
								"EDUCATIONAL_INSTITUTION",
								item,
								"EDUCATION"
							);
						}

						if (!validation_successed) {
							matched = false;
						} else {
						}

						continue;
					}

					if (Array.isArray(filters[f])) {
						const [min, max] = filters[f];

						if (!(item[f] >= min && item[f] <= max)) {
							matched = false;
						}

						continue;
					}

					if (item[f] != filters[f]) {
						matched = false;
					}
					// console.log(
					//   "filtering list",
					//   item,
					//   item[f] != filters[f],
					//   item[f],
					//   filters[f],
					//   f
					// );
				}
			}

			return matched;
		});
		commit(
			"setDatalist",
			new_datalist.filter((v, i, a) => a.indexOf(v) === i)
		);
		// console.log("result", result);
		commit("setResultList", result);
	},

	setAuthUser({ commit, dispatch }, data) {
		commit("setAuthUser", data);
		dispatch("fetchFilters");
	},
	async fetchAd({ commit }) {
		let url = process.server
			? "https://api.yedilov.online/rest/adw/"
			: "/api/rest/adw/";
		this.$axios.setHeader("Access-Control-Allow-Origin", "*");
		this.$axios.setHeader("X-Requested-With", "XMLHttpRequest");
		await this.$axios
			.$get(url)
			.then((response) => {
				return response.result;
			})
			.then((data) => {
				commit("setAd", data);
			});
	},
	setFormStatusAction({ commit }, data) {
		commit("setFormStatus", data);
	},
	async verifyPhone({ commit }, data) {
		try {
			const url = process.server
				? "https://api.yedilov.online/mobileapp/api/verifyphone"
				: "/api/mobileapp/api/verifyphone";

			commit("setFormStatus", {
				name: "VERIFYPHONE",
				loading: true,
			});

			const response = await this.$axios.$post(url, JSON.stringify(data));
			console.log(response, "RESPONSE");
			if (!response) {
				throw new Error();
			} else if (response.errors) {
				const { message } = response.errors[0];

				throw new Error(message);
			}
			if (response.data) {
				commit("setFormStatus", {
					name: "VERIFYPHONE",
					loading: false,
					status: 200,
					message: "",
				});
				commit("setAuthUser", {
					...data,
					code: response.data,
				});
			}
		} catch (error) {
			commit("setFormStatus", {
				name: "VERIFYPHONE",
				status: 500,
				loading: false,
				message: error.message || "Что-то пошло не так",
			});
		}
	},
	async registration({ commit }, data) {
		try {
			const url = process.server
				? "https://api.yedilov.online/mobileapp/api/register"
				: "/api/mobileapp/api/register";

			commit("setFormStatus", {
				name: "REGISTRATION",
				loading: true,
			});

			const response = await this.$axios.$post(url, JSON.stringify(data));
			if (!response) {
				throw new Error();
			} else {
				if (response.errors) {
					const { message } = response.errors[0];

					throw new Error(message);
				}
			}
			if (response.token) {
				commit("setFormStatus", {
					name: "REGISTRATION",
					loading: false,
					status: 200,
					message: "Регистрация прошла успешно!",
				});

				const { phone } = data;

				localStorage.setItem("signed", true);
				localStorage.setItem("login", phone);
				localStorage.setItem("token", response.token);
				localStorage.setItem("refresh_token", response.refresh_token);
				commit("setAuthUser", {
					signed: true,
					login: phone,
					...response.data,
				});
			}
		} catch (error) {
			commit("setFormStatus", {
				name: "REGISTRATION",
				status: 500,
				loading: false,
				message: error.message || "Что-то пошло не так",
			});
		}
	},
	async login({ commit }, data) {
		try {
			const url = process.server
				? "https://api.yedilov.online/mobileapp/api/signin"
				: "/api/mobileapp/api/signin";

			commit("setFormStatus", {
				name: "LOGIN",
				loading: true,
			});
			console.log(JSON.stringify(data));
			const response = await this.$axios.$post(url, JSON.stringify(data));
			console.log("0", 1);
			if (!response) {
				console.log("1", 1);
				throw new Error();
			} else {
				console.log("1,5", 1);
				if (response.errors) {
					const { message } = response.errors[0];
					console.log("2", message);
					throw new Error(message);
				}
			}
			if (response.token) {
				console.log("4", 2);
				commit("setFormStatus", {
					name: "LOGIN",
					loading: false,
					status: 200,
					message: "Авторизация прошла успешно!",
				});

				const { login } = data;

				localStorage.setItem("signed", true);
				localStorage.setItem("login", login);
				localStorage.setItem("token", response.token);
				localStorage.setItem("refresh_token", response.refresh_token);
				commit("setAuthUser", {
					signed: true,
					login,
					...response.data,
				});
			}
		} catch (error) {
			console.log(
				"3catching rrorr",
				error,
				JSON.stringify(data),
				error.message
			);
			commit("setFormStatus", {
				name: "LOGIN",
				status: 500,
				loading: false,
				message: error.message || "Что-то пошло не так",
			});
		}
	},
	async forgot({ commit }, data) {
		try {
			console.log("RECOVERIGN", data);
			const url = process.server
				? "https://api.yedilov.online/mobileapp/api/recover"
				: "/api/mobileapp/api/recover";

			commit("setFormStatus", {
				name: "LOGIN",
				loading: true,
			});

			const response = await this.$axios.$post(url, data);
			console.log("REsponse tokeN", url);
			console.log("633", url);
			if (!response) {
				console.log("6", url);
				throw new Error();
			} else {
				if (response.errors) {
					console.log("7", url);
					const { message } = response.errors[0];

					throw new Error(message);
				}
			}
			console.log("336", url);

			console.log("5", url);
			commit("setFormStatus", {
				name: "LOGIN",
				loading: false,
				status: 200,
				message: "Восстановление пароля прошла успешно!",
			});
		} catch (error) {
			console.log("8", error);
			commit("setFormStatus", {
				name: "LOGIN",
				status: 500,
				loading: false,
				message: error.message || "Что-то пошло не так",
			});
		}
	},
	async profileEdit({ commit }, data) {
		try {
			const url = process.server
				? "https://api.yedilov.online/mobileapp/api/profile/edit"
				: "/api/mobileapp/api/profile/edit";

			commit("setFormStatus", {
				name: "PROFILE",
				loading: true,
			});

			const token = localStorage.getItem("token");

			this.$axios.setHeader("Token", token);

			const response = await this.$axios.$post(url, JSON.stringify(data));
			if (!response) {
				throw new Error();
			} else {
				if (response.errors) {
					const { message } = response.errors[0];

					throw new Error(message);
				}
			}
			if (response.success) {
				commit("setFormStatus", {
					name: "PROFILE",
					loading: false,
					status: 200,
					message: "Профиль изменен успешно!",
				});
			}
		} catch (error) {
			commit("setFormStatus", {
				name: "PROFILE",
				status: 500,
				loading: false,
				message: error.message || "Что-то пошло не так",
			});
		}
	},
	async fetchTariff({ commit }, data) {
		try {
			const url = process.server
				? "https://api.yedilov.online/mobileapp/api/tariff"
				: "/api/mobileapp/api/tariff";
			const response = await this.$axios.$get(url, JSON.stringify(data));
			if (response) {
				if (response.data) {
					commit("setTariffList", response.data);
				}
			}
		} catch (error) {}
	},
	async fetchFAQList({ commit }, data) {
		try {
			const url = process.server
				? "https://api.yedilov.online/mobileapp/api/faqpurchase"
				: "/api/mobileapp/api/faqpurchase";
			const response = await this.$axios.$get(url, JSON.stringify(data));
			if (response) {
				if (response.data) {
					commit("setFAQList", response.data);
				}
			}
		} catch (error) {}
	},
};
