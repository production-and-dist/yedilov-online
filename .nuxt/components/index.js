export { default as Filters } from '../..\\components\\filters.vue'
export { default as Loader } from '../..\\components\\loader.vue'
export { default as Loading } from '../..\\components\\loading.vue'
export { default as Logo } from '../..\\components\\logo.vue'
export { default as MobileFilter } from '../..\\components\\mobile-filter.vue'
export { default as MobileMenu } from '../..\\components\\mobile-menu.vue'
export { default as ModalAd } from '../..\\components\\modal-ad.vue'
export { default as ModalAuth } from '../..\\components\\modal-auth.vue'
export { default as ModalForgetPassword } from '../..\\components\\modal-forgetPassword.vue'
export { default as ModalForgetPasswordVerifyPhone } from '../..\\components\\modal-forgetPasswordVerifyPhone.vue'
export { default as ModalPurchase } from '../..\\components\\modal-purchase.vue'
export { default as ModalRegistration } from '../..\\components\\modal-registration.vue'
export { default as ModalVerifyphone } from '../..\\components\\modal-verifyphone.vue'
export { default as Modal } from '../..\\components\\modal.vue'
export { default as PersonCareer } from '../..\\components\\person-career.vue'
export { default as PersonEducation } from '../..\\components\\person-education.vue'
export { default as PersonProfile } from '../..\\components\\person-profile.vue'
export { default as Result } from '../..\\components\\result.vue'
export { default as Search } from '../..\\components\\search.vue'
export { default as StandardLoader } from '../..\\components\\standard-loader.vue'

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
