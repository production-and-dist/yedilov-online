# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<Filters>` | `<filters>` (components/filters.vue)
- `<Loader>` | `<loader>` (components/loader.vue)
- `<Loading>` | `<loading>` (components/loading.vue)
- `<Logo>` | `<logo>` (components/logo.vue)
- `<MobileFilter>` | `<mobile-filter>` (components/mobile-filter.vue)
- `<MobileMenu>` | `<mobile-menu>` (components/mobile-menu.vue)
- `<ModalAd>` | `<modal-ad>` (components/modal-ad.vue)
- `<ModalAuth>` | `<modal-auth>` (components/modal-auth.vue)
- `<ModalForgetPassword>` | `<modal-forget-password>` (components/modal-forgetPassword.vue)
- `<ModalForgetPasswordVerifyPhone>` | `<modal-forget-password-verify-phone>` (components/modal-forgetPasswordVerifyPhone.vue)
- `<ModalPurchase>` | `<modal-purchase>` (components/modal-purchase.vue)
- `<ModalRegistration>` | `<modal-registration>` (components/modal-registration.vue)
- `<ModalVerifyphone>` | `<modal-verifyphone>` (components/modal-verifyphone.vue)
- `<Modal>` | `<modal>` (components/modal.vue)
- `<PersonCareer>` | `<person-career>` (components/person-career.vue)
- `<PersonEducation>` | `<person-education>` (components/person-education.vue)
- `<PersonProfile>` | `<person-profile>` (components/person-profile.vue)
- `<Result>` | `<result>` (components/result.vue)
- `<Search>` | `<search>` (components/search.vue)
- `<StandardLoader>` | `<standard-loader>` (components/standard-loader.vue)
